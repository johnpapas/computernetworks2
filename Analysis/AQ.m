%AQ Analysis

%Variables
diffs1V=importdata('differencesAQ(Apres la pluie II).dat');
diffs2V=importdata('differencesAQ(Keli 33).dat');
values1V=importdata('valuesAQ(Apres la pluie II).dat');
values2V=importdata('valuesAQ(Keli 33).dat');
meanstep1M=importdata('quantizerAQ(Apres la pluie II).dat');
meanstep2M=importdata('quantizerAQ(Keli 33).dat');
mean1V=meanstep1M(:,1);
step1V=meanstep1M(:,2);
mean2V=meanstep2M(:,1);
step2V=meanstep2M(:,2);
binEdges=-8:7;


%Plotting
figure(1)
clf
histogram(diffs1V,'BinEdges', binEdges)
xlabel("Value of difference");
ylabel("Number of times appeared");
title("Frequency of difference values (Song: Apres la pluie II)");

figure(2)
clf
histogram(values1V)
xlabel("Values");
ylabel("Number of times appeared");
title("Frequency of values (Song: Apres la pluie II)");

figure(3)
clf
scatter((1:length(mean1V)), mean1V, '.')
title("Mean of quantizer (Song: Apres la pluie II)")
xlabel("# of packet");
ylabel("Mean");

figure(4)
clf
scatter((1:length(step1V)), step1V, '.')
title("Step of quantizer (Song: Apres la pluie II)")
xlabel("# of packet");
ylabel("Value of step");

figure(5)
clf
scatter((1:length(mean2V)), mean2V, '.')
title("Mean of quantizer (Song: ��� ���� 33)")
xlabel("# of packet");
ylabel("Mean");

figure(6)
clf
scatter((1:length(step2V)), step2V, '.')
title("Step of quantizer (Song: ��� ���� 33)")
xlabel("# of packet");
ylabel("Value of step");

