%Analyzing the DPCM

%Variables
diffsFreq=importdata('differences_freq(296-297Hz).dat');
diffsMusic=importdata('differences_music.dat');
valuesFreq=importdata('values_freq.dat');
valuesMusic=importdata('values_music(Keli 33).dat');
binEdges=-8:7;

%plotting
figure(1)
clf
plot(valuesFreq)
title("Frequency (296-297Hz)");
xlabel("Seconds");
ylabel("Value");

figure(2)
clf
plot(valuesMusic)
title("Music (Song: ��� ���� 33)");
xlabel("Seconds");
ylabel("Value");

figure(3)
clf
histogram(diffsMusic,'BinEdges', binEdges)
xlabel("Value of difference");
ylabel("Number of times appeared");
title("Frequency of difference values (Song: ��� ���� 33)");

figure(4)
clf
histogram(valuesMusic)
xlabel("Values");
ylabel("Number of times appeared");
title("Frequency of values (Song: ��� ���� 33)");
