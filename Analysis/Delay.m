%Analyzing the echo packets with delays

%Variables
delayPings=importdata('Delay_E7301.dat');
throughputDelay=importdata('throughput_Delay_E7301.dat');
%binEdges=min(delayPings):50;

%Plotting

figure(1)
clf
plot(delayPings)
xlabel("# of packet");
ylabel("Ping value (ms)");
title("Response time of packets with delay (E7301)");
hold on
plot(1:length(delayPings), ones(length(delayPings),1)*mean(delayPings), '-');
legend({"Ping value", strcat("Mean Ping=", num2str(mean(delayPings)))}, 'Location', 'northwest')

figure(2)
clf
plot(throughputDelay)
xlabel("Seconds");
ylabel("Throughput (bytes/s)");
title("System Throughput every second (E7301)");
hold on

plot(1:length(throughputDelay), ones(length(throughputDelay),1)*mean(throughputDelay), '-');
legend({"Throughput value", strcat("Mean Throughput=", num2str(mean(throughputDelay)))}, 'Location', 'southeast')

figure(3)
clf
histogram(delayPings);
ylabel("Appearing frequency");
xlabel("Value of ping");
title("Frequency of value of ping to appear");

figure(4)
clf
histogram(throughputDelay);
ylabel("Appearing frequency");
xlabel("Value of throughput");
title("Frequency of value of throughput to appear");


