%Analyzing the echo packets without delays

%Variables
noDelayPings=importdata('NoDelay_E0000.dat');
throughputNoDelay=importdata('throughput_NoDelay_E0000.dat');
binEdges=min(noDelayPings):50;

%Plotting

figure(1)
clf
plot(noDelayPings)
xlabel("# of packet");
ylabel("Ping value (ms)");
title("Response time of packets without delay");
hold on
plot(1:length(noDelayPings), ones(length(noDelayPings),1)*mean(noDelayPings), '-');
legend({"Ping value", strcat("Mean Ping=", num2str(mean(noDelayPings)))}, 'Location', 'northeast')

figure(2)
clf
plot(throughputNoDelay)
xlabel("Seconds");
ylabel("Throughput (bytes/s)");
title("System Throughput every second");
hold on
plot(1:length(throughputNoDelay), ones(length(throughputNoDelay),1)*mean(throughputNoDelay), '-');
legend({"Throughput value", strcat("Mean Throughput=", num2str(mean(throughputNoDelay)))}, 'Location', 'southeast')

figure(3)
clf
histogram(noDelayPings, 'BinEdges', binEdges);
ylabel("Appearing frequency");
xlabel("Value of ping");
title("Frequency of value of ping to appear");

figure(4)
clf
histogram(throughputNoDelay);
ylabel("Appearing frequency");
xlabel("Value of throughput");
title("Frequency of value of throughput to appear");





