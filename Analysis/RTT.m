%The RTT analysis

%Variables
a=0.875;
b=0.75;
c=2;
rttV=importdata('Delay_E5599.dat');
SRTT=zeros(length(rttV),1);
var=zeros(length(rttV),1);
SRTT(1)=rttV(1);
var(1)=rttV(1)/2;

%SRTT and variance
for(i=2:length(SRTT))
    SRTT(i)=SRTT(i-1)*a+(1-a)*rttV(i-1);
    var(i)=var(i-1)*b+(1-b)*abs(SRTT(i-1)-rttV(i-1));
end

%Time-Out
RTO=SRTT+c*var;

%plotting
figure(1)
clf
plot(SRTT);
hold on
plot(1:length(SRTT), ones(length(SRTT),1)*mean(SRTT), '--');
hold on
plot(var);
hold on
plot(1:length(var), ones(length(var),1)*mean(var), '--');
hold on
plot(RTO);
hold on 
plot(1:length(RTO), ones(length(RTO),1)*mean(RTO), '--');
xlabel("# of packet")
ylabel("Milliseconds")
title("Values of smooth round trip time, its variance and retransmission timeout")
legend({"SRTT", "Mean SRTT", "Variance", "Mean Variance", "Retransmission Timeout", "Mean Retransmission Timeout"}, 'NumColumns',2);
text((length(SRTT)+1), mean(SRTT), num2str(mean(SRTT)),'FontSize',13)
text((length(var)+1), mean(var), num2str(mean(var)),'FontSize',13)
text((length(RTO)+1), mean(RTO), num2str(mean(RTO)),'FontSize',13)

