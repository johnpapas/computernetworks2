%Copter Analysis

%Variables
autoflight300=importdata('copterData300(��������� ����������).dat');
autoflight500=importdata('copterData500AUTOFLIGHT.dat');

%plotting
figure(1)
clf
plot(autoflight300(:,1));
title("Altitude of ithakicopter-AUTOFLIGHT LEVEL=300");
ylabel("Altitude")
xlabel("Seconds")

figure(2)
clf
plot(autoflight500(:,1));
title("Altitude of ithakicopter-AUTOFLIGHT LEVEL=500");
ylabel("Altitude")
xlabel("Seconds")