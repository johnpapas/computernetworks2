%Vehicle analysis

%Variables
dataM=importdata('vehicleData.dat');
engineRunTimeYY=dataM(:,2);
engineRunTimeXX=dataM(:,1);
airTemp=dataM(:,3);
throttlePos=dataM(:,4);
engineRPMXX=dataM(:,5);
engineRPMYY=dataM(:,6);
vehicleSpeed=dataM(:,7);
coolantTemp=dataM(:,8);
k=1;

engineRunTime=256*engineRunTimeXX+engineRunTimeYY;
airTemperature=airTemp-40;
throttlePosition=(throttlePos*100)./255;
coolTemp=coolantTemp-40;
engineRPM=((engineRPMXX*256)+engineRPMYY)/4;

%Find 4 minutes run time
for(i=k:length(engineRunTime))
    if(engineRunTime(i)-engineRunTime(k)>240)
        index=i;
        break;
    end
end

%plotting
figure(1)
clf
plot(engineRunTime(k:index), airTemperature(k:index));
xlabel("Seconds")
ylabel("Air Temperature (C)")
title("Temperature of air for 4 minutes runtime")

figure(2)
clf
plot(engineRunTime(k:index), throttlePosition(k:index));
xlabel("Seconds")
ylabel("Position of throttle (%)")
title("Throttle position for 4 minutes runtime")

figure(3)
clf
plot(engineRunTime(k:index), coolTemp(k:index));
xlabel("Seconds")
ylabel("Coolant Temperature (C)")
title("Temperature of coolant for 4 minutes runtime")

figure(4)
clf
plot(engineRunTime(k:index), vehicleSpeed(k:index));
xlabel("Seconds")
ylabel("Speed Vehicle (km/h)")
title("Speed of the vehicle for 4 minutes runtime")

figure(5)
clf
plot(engineRunTime(k:index), engineRPM(k:index));
xlabel("Seconds")
ylabel("Rounds per minute")
title("RPM of the engine for 4 minutes runtime")