package projectCN2;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.SourceDataLine;

/**
 * A project for learning usage of UDP and TCP/IP connections Computer Networks
 * 2
 * 
 * @author Papas Ioannis
 * 
 */

public class Main {

	private static final int serverListeningPort = 38020;

	private static final int clientListeningPort = 48020;

	private static final String echoRequestCode = "E5599";

	private static final String imageRequestCode = "M4616";

	private static final String audioRequestCode = "A7249";

	public static void main(String[] args) {

		System.out.println("Write a number: 1 for Echo Delay\n" + "2 for Echo without delay\n" + "3 for temperature\n"
				+ "4 for image\n" + "5 for music with DPCM\n" + "6 for frequency generator\n"
				+ "7 for music with AQDPCM\n" + "8 for measurements from ithakicopter\n"
				+ "9 for measurements from the vehicle");

		String userInput = input();

		switch (userInput) {
		case "1":
			echo("echodelay", 300000); // for 5 minutes
			break;
		case "2":
			echo("echonodelay", 300000); // for 5 minutes
			break;
		case "3":
			temperature(); // run at the beginning of session and at the end
			break;
		case "4":
			image("CAM=PTZ");
			break;
		case "5":
			audioDPCM("music");
			break;
		case "6":
			audioDPCM("frequency generator");
			break;
		case "7":
			audioAQDPCM();
			break;
		case "8":
			ithakicopter(450, 200, 200, 100);
			break;
		case "9":
			vehicle(350);
			break;
		default:
			System.out.println("Wrong input, please try again...");
			break;
		}

	}

	/**
	 * Sends request for echo packets and calculates the time of the response and
	 * the system througput. Then writes the pings and the throughput in a file
	 * 
	 * @param message - "echodelay" for the packets to have delay from ithaki or
	 *                echonodelay for calculating the classic response time
	 * @param time    - the time in which you want to calculate the response time
	 * 
	 */

	public static void echo(String message, long time) {
		byte[] txdata = new byte[128];
		byte[] rxdata = new byte[128];
		String packetInfo = "";
		String fileName = "";
		if (message == "echodelay") {
			packetInfo = echoRequestCode;
			fileName = "Delay_" + echoRequestCode + ".dat";
		} else if (message == "echonodelay") {
			packetInfo = "E0000";
			fileName = "NoDelay_E0000.dat";
		}
		txdata = packetInfo.getBytes();
		try {
			InetAddress hostAddress = InetAddress.getByName("ithaki.eng.auth.gr");
			DatagramSocket s, r;
			s = new DatagramSocket();
			DatagramPacket p = new DatagramPacket(txdata, txdata.length, hostAddress, serverListeningPort);
			r = new DatagramSocket(clientListeningPort);
			DatagramPacket q = new DatagramPacket(rxdata, rxdata.length);
			r.setSoTimeout(2000);
			long timePassed = 0;
			long timePassed2 = 0;
			int counter = 0;
			ArrayList<Long> pings = new ArrayList<Long>();
			ArrayList<Integer> pps = new ArrayList<Integer>();
			ArrayList<Long> times = new ArrayList<Long>();
			System.out.println("Sending echo packets and saving pings...");
			while (timePassed < time) {
				long begin = System.currentTimeMillis();
				try {
					s.send(p);
					r.receive(q);
					counter++;
					// String message1=new String(rxdata,0,q.getLength());
					long end = System.currentTimeMillis();
					// System.out.println(message1 + "\n" + counter);
					// System.out.println(end-begin);
					pings.add(end - begin);
					timePassed += (end - begin);
					timePassed2 += (end - begin);
					if (timePassed2 > 1000) {
						// System.out.println(counter);
						times.add(timePassed2);
						timePassed2 = 0;
						pps.add(counter);
						counter = 0;
					}
				} catch (Exception x) {
					System.out.println("No more data to fetch");
					break;
				}
			}

			try {
				File pingsFile = new File(fileName);
				FileOutputStream out = new FileOutputStream(pingsFile);
				for (int i = 0; i < pings.size(); i++) {
					out.write((Long.toString(pings.get(i)) + "\n").getBytes());
				}
				System.out.println("Pings saved at " + pingsFile.getCanonicalPath());
				File throughputFile = new File("throughput_" + fileName);
				FileOutputStream out1 = new FileOutputStream(throughputFile);
				for (int i = 7; i < pps.size(); i++) {
					double k = 0;
					long t = 0;
					for (int j = 0; j < 8; j++) {
						k += pps.get(i - j);
						t += times.get(i - j);
					}
					k = 128 * 8 * 1000 * k / (int) t;
					out1.write((Double.toString(k) + "\n").getBytes());
				}
				System.out.println("Throughputs saved at " + pingsFile.getCanonicalPath());
				out1.close();
				out.close();
			} catch (Exception x) {
				System.out.println("Error writing files...");
			}
			s.close();
			r.close();
		} catch (Exception x) {
			System.out.println("Socket reached timeout. Closing socket...");
		}
	}

	/**
	 * Sends request for an echo packet that contains the temperature and then
	 * writes the temperature in a file
	 * 
	 */

	public static void temperature() {
		byte[] txdata = new byte[128];
		byte[] rxdata = new byte[128];
		String packetInfo = echoRequestCode + "T00";
		txdata = packetInfo.getBytes();
		try {
			InetAddress hostAddress = InetAddress.getByName("ithaki.eng.auth.gr");
			DatagramSocket s, r;
			s = new DatagramSocket();
			DatagramPacket p = new DatagramPacket(txdata, txdata.length, hostAddress, serverListeningPort);
			r = new DatagramSocket(clientListeningPort);
			DatagramPacket q = new DatagramPacket(rxdata, rxdata.length);
			r.setSoTimeout(2000);
			String message = "";
			System.out.println("Fetching value of temperature from station 00...");
			try {
				s.send(p);
				r.receive(q);
				message = new String(rxdata, 0, q.getLength());
				// System.out.println(message + "\n");
			} catch (Exception x) {
				System.out.println("Error fetching data. Closing socket...\n");
			}
			String[] splittedMessage = message.split("\\s+");
			try {
				File temperatureFile = new File("temperature_" + echoRequestCode + "T00.txt");
				FileOutputStream out = new FileOutputStream(temperatureFile);
				out.write(splittedMessage[6].getBytes());
				System.out.println("Temperature value saved at " + temperatureFile.getCanonicalPath());
				out.close();
			} catch (Exception x) {
				System.out.println("�rror while creating file...");
			}
			s.close();
			r.close();
		} catch (Exception x) {
			System.out.println("Socket reached timeout. Closing socket...");
		}
	}

	/**
	 * Sends a request for receiving an image and then saves the image at the
	 * directory of the src
	 * 
	 * @param message - "CAM=PTZ" for the first camera, "CAM=FIX" for the other
	 * 
	 */

	public static void image(String message) {
		byte[] txdata = new byte[128];
		byte[] rxdata = new byte[1024];
		String packetInfo = imageRequestCode + "UDP=1024" + message;
		txdata = packetInfo.getBytes();
		try {
			InetAddress hostAddress = InetAddress.getByName("ithaki.eng.auth.gr");
			DatagramSocket s, r;
			s = new DatagramSocket();
			DatagramPacket p = new DatagramPacket(txdata, txdata.length, hostAddress, serverListeningPort);
			r = new DatagramSocket(clientListeningPort);
			DatagramPacket q = new DatagramPacket(rxdata, rxdata.length);
			r.setSoTimeout(2000);
			s.send(p);
			FileOutputStream out = null;
			String fileName = "image.jpeg";
			File imageFile = new File(fileName);
			System.out.println("Saving image...");
			try {
				out = new FileOutputStream(imageFile);
				while (true) {
					try {
						if (q.getLength() == 0)
							break;
						r.receive(q);
						out.write(rxdata);
					} catch (Exception x) {
						System.out.println(x);
						break;
					}

				}
			} catch (Exception x) {
				System.out.println("�� data to fetch. Image finished");
			}
			System.out.println("Image saved at " + imageFile.getCanonicalPath());
			s.close();
			r.close();
		} catch (Exception x) {
			System.out.println("Socket reached timeout. Closing socket...");
		}
	}

	/**
	 * 
	 * Sends an request for audio from the ithaki server. It plays the audio sent
	 * immediately and writes the values played and the data sent (differences) in 2
	 * separate files. The receiveThread receives the data and edits it to the
	 * proper form (decoding) while the playThread uses lineOut to play the
	 * audioclip
	 * 
	 * @param message -"frequency generator" for sounds from a frequency generator
	 *                or "music" for a music clip from the playlist of ithaki
	 * 
	 */

	public static void audioDPCM(String message) {
		byte[] txdata = new byte[128];
		byte[] rxdata = new byte[128];
		String packetInfo = "";
		String fileName = "";
		if (message == "frequency generator") {
			packetInfo = audioRequestCode + "T500";
			fileName = "freq";
		} else if (message == "music") {
			packetInfo = audioRequestCode + "F900";
			fileName = "music";
		}
		txdata = packetInfo.getBytes();
		try {
			InetAddress hostAddress = InetAddress.getByName("ithaki.eng.auth.gr");
			DatagramSocket s, r;
			s = new DatagramSocket();
			DatagramPacket p = new DatagramPacket(txdata, txdata.length, hostAddress, serverListeningPort);
			r = new DatagramSocket(clientListeningPort);
			DatagramPacket q = new DatagramPacket(rxdata, rxdata.length);
			r.setSoTimeout(2000);
			s.send(p);
			AudioFormat linearPCM = new AudioFormat(8000, 8, 1, true, false);
			SourceDataLine lineOut = AudioSystem.getSourceDataLine(linearPCM);
			lineOut.open(linearPCM);
			lineOut.start();
			BlockingQueue<byte[]> audioFile = new LinkedBlockingQueue<byte[]>();
			ArrayList<Integer> values = new ArrayList<Integer>();
			ArrayList<Integer> diffs = new ArrayList<Integer>();

			Thread receiveThread = new Thread() {
				@Override
				public void run() {
					int nibble = 0;
					int diff = 0;
					while (q.getLength() != 0) {
						nibble = 0;
						try {
							byte[] temp = new byte[256];
							r.receive(q);

							for (int i = 0, j = 0; i < rxdata.length; i++, j = j + 2) {
								diff = ((rxdata[i] >>> 4) & 0x0F) - 8;
								diffs.add(diff);
								temp[j] = (byte) (diff + nibble);
								// System.out.println(diff);
								if (temp[j] > 127) {
									temp[j] = (byte) 127;
								}
								if (temp[j] < -128) {
									temp[j] = (byte) (-128);
								}
								values.add((int) temp[j]);
								diff = (rxdata[i] & (byte) 0x0F) - 8;
								diffs.add(diff);
								temp[j + 1] = (byte) (diff + temp[j]);
								// System.out.println(diff);
								if (temp[j + 1] > 127) {
									temp[j + 1] = (byte) 127;
								}
								if (temp[j + 1] < -128) {
									temp[j + 1] = (byte) (-128);
								}
								nibble = temp[j + 1];
								values.add((int) temp[j + 1]);
							}
							audioFile.put(temp);

						} catch (Exception x) {
							// x.printStackTrace();
							break;
						}
					}
				}
			};

			Thread playThread = new Thread() {
				@Override
				public void run() {
					while (true) {
						try {
							byte[] temp = audioFile.poll(100, TimeUnit.MILLISECONDS);
							// out.write(temp, 0, 128);
							if (temp == null) {
								break;
							}
							lineOut.write(temp, 0, temp.length);

						} catch (Exception x) {
							// x.printStackTrace();
							break;
						}
					}

				}
			};
			receiveThread.start();
			playThread.start();
			receiveThread.join();
			playThread.join();

			lineOut.stop();
			lineOut.close();

			try {
				File diffsFile = new File("differences_" + fileName + ".dat");
				FileOutputStream out = new FileOutputStream(diffsFile);
				for (int i = 0; i < diffs.size(); i++) {
					out.write((Integer.toString(diffs.get(i)) + "\n").getBytes());
				}
				System.out.println("Differences saved at " + diffsFile.getCanonicalPath());
				File valuesFile = new File("values_" + fileName + ".dat");
				FileOutputStream out1 = new FileOutputStream(valuesFile);
				for (int i = 0; i < values.size(); i++) {
					out1.write((Integer.toString(values.get(i)) + "\n").getBytes());
				}
				System.out.println("Values saved at " + valuesFile.getCanonicalPath());
				out1.close();
				out.close();
			} catch (Exception x) {
				System.out.println("Error writing files...");
			}

			s.close();
			r.close();

		} catch (Exception x) {
			System.out.println("Socket reached timeout. Closing socket...");
		}
	}

	/**
	 * 
	 * Sends an request for audio from the ithaki server. It plays the audio sent
	 * immediately and writes the values played and the data sent (differences) in 2
	 * separate files. The receiveThread receives the data and edits it to the
	 * proper form (decoding) while the playThread uses lineOut to play the
	 * audioclip. The decoding part is different from the previous function because
	 * this encoding is different (AQDPCM). It also saves the mean and step
	 * quantizer in a file
	 * 
	 */

	public static void audioAQDPCM() {
		byte[] txdata = new byte[128];
		byte[] rxdata = new byte[132];
		String packetInfo = audioRequestCode + "AQF920";
		txdata = packetInfo.getBytes();
		try {
			InetAddress hostAddress = InetAddress.getByName("ithaki.eng.auth.gr");
			DatagramSocket s, r;
			s = new DatagramSocket();
			DatagramPacket p = new DatagramPacket(txdata, txdata.length, hostAddress, serverListeningPort);
			r = new DatagramSocket(clientListeningPort);
			DatagramPacket q = new DatagramPacket(rxdata, rxdata.length);
			r.setSoTimeout(2000);
			s.send(p);
			AudioFormat linearPCM = new AudioFormat(8000, 16, 1, true, false);
			SourceDataLine lineOut = AudioSystem.getSourceDataLine(linearPCM);
			lineOut.open(linearPCM);
			lineOut.start();
			BlockingQueue<byte[]> audioFile = new LinkedBlockingQueue<byte[]>();
			ArrayList<Integer> diffs = new ArrayList<Integer>();
			ArrayList<Integer> step = new ArrayList<Integer>();
			ArrayList<Integer> means = new ArrayList<Integer>();
			ArrayList<Integer> values = new ArrayList<Integer>();

			Thread receiveThread = new Thread() {
				@Override
				public void run() {
					int nibble = 0;
					while (true) {
						try {
							byte[] temp = new byte[256 * 2];
							r.receive(q);
							byte[] bb = new byte[4];
							byte sign = (byte) ((rxdata[1] & 0x80) != 0 ? 0xff : 0x00);
							bb[3] = sign;
							bb[2] = sign;
							bb[1] = rxdata[1];
							bb[0] = rxdata[0];

							int m = ByteBuffer.wrap(bb).order(ByteOrder.LITTLE_ENDIAN).getInt();
							means.add(m);

							sign = (byte) ((rxdata[3] & 0x80) != 0 ? 0xff : 0x00);
							bb[3] = sign;
							bb[2] = sign;
							bb[1] = rxdata[3];
							bb[0] = rxdata[2];

							int b = ByteBuffer.wrap(bb).order(ByteOrder.LITTLE_ENDIAN).getInt();
							step.add(b);

							for (int i = 4, j = 0; i < rxdata.length; i++, j = j + 4) {
								int D1 = ((rxdata[i] >>> 4) & 0x0f);
								int D2 = rxdata[i] & 0x0f;

								int d1 = D1 - 8;
								int d2 = D2 - 8;
								diffs.add(d1);
								diffs.add(d2);

								int delta1 = d1 * b;
								int delta2 = d2 * b;

								int X1 = delta1 + nibble;
								int X2 = delta2 + delta1;
								nibble = delta2;

								int x1 = X1 + m;
								int x2 = X2 + m;

								temp[j] = (byte) (x1);
								values.add(x1);
								temp[j + 1] = (byte) (x1 / 256 > 127 ? 127 : x1 / 256 < -128 ? -128 : x1 / 256);
								temp[j + 2] = (byte) (x2);
								values.add(x2);
								temp[j + 3] = (byte) (x2 / 256 > 127 ? 127 : x2 / 256 < -128 ? -128 : x2 / 256);

							}
							audioFile.put(temp);

						} catch (Exception x) {
							x.printStackTrace();
							break;
						}
					}

				}
			};

			Thread playThread = new Thread() {
				@Override
				public void run() {
					// ByteArrayOutputStream out=new ByteArrayOutputStream();
					while (true) {
						try {
							byte[] temp = audioFile.poll(100, TimeUnit.MILLISECONDS);
							// out.write(temp, 0, 128);
							lineOut.write(temp, 0, temp.length);
						} catch (Exception x) {
							x.printStackTrace();
							break;
						}
					}
				}
			};

			receiveThread.start();
			playThread.start();
			receiveThread.join();
			playThread.join();

			lineOut.stop();
			lineOut.close();

			try {
				File diffsFile = new File("differencesAQ.dat");
				FileOutputStream out = new FileOutputStream(diffsFile);
				for (int i = 0; i < diffs.size(); i++) {
					out.write((Integer.toString(diffs.get(i)) + "\n").getBytes());
				}
				System.out.println("Differences from AQ saved at " + diffsFile.getCanonicalPath());
				File valuesFile = new File("valuesAQ.dat");
				FileOutputStream out1 = new FileOutputStream(valuesFile);
				for (int i = 0; i < values.size(); i++) {
					out1.write((Integer.toString(values.get(i)) + "\n").getBytes());
				}
				System.out.println("Values saved at " + valuesFile.getCanonicalPath());
				File quantizerFile = new File("quantizerAQ.dat");
				FileOutputStream out2 = new FileOutputStream(quantizerFile);
				for (int i = 0, j = 0; i < values.size() && j < step.size(); i++, j++) {
					out2.write(
							(Integer.toString(means.get(i)) + " " + Integer.toString(step.get(j)) + "\n").getBytes());
				}
				System.out.println("Values of mean and step quantization saved at " + quantizerFile.getCanonicalPath());
				out2.close();
				out1.close();
				out.close();
			} catch (Exception x) {
				x.printStackTrace();
			}

			s.close();
			r.close();

		} catch (Exception x) {
			System.out.println("Socket reached timeout. Closing socket...");
		}
	}

	/**
	 * 
	 * Sends nTimesSent requests while changing the power of the motors in different
	 * ways. Then saves the altitude in a file. Using TCP
	 * 
	 * @param flightLevel -the desired flightlevel (200-500)
	 * @param leftMotor   -power of the left motor (150-200)
	 * @param rightMotor  -power of the right motor (150-200)
	 * @param nTimesSent  -how many times to send a control request (0-500)
	 */

	public static void ithakicopter(int flightLevel, int leftMotor, int rightMotor, int nTimesSent) {

		ArrayList<Integer> altitude = new ArrayList<Integer>();
		ArrayList<Double> pressure = new ArrayList<Double>();
		String message = "";

		try {
			Socket s = new Socket();
			InetAddress hostAddress = InetAddress.getByName("ithaki.eng.auth.gr");
			InetSocketAddress socketAddress = new InetSocketAddress(hostAddress, 38048);
			s.connect(socketAddress, 2000);
			InputStream in = s.getInputStream();
			OutputStream out = s.getOutputStream();
			for (int i = 0; i < nTimesSent; i++) {
				// changing the power of the motors
				if (i > nTimesSent / 2) {
					leftMotor = 180;
					rightMotor = 180;
				}
				out.write(
						("AUTO FLIGHTLEVEL=" + Integer.toString(flightLevel) + " LMOTOR=" + Integer.toString(leftMotor)
								+ " RMOTOR=" + Integer.toString(rightMotor) + " PILOT \r\n").getBytes());
			}
			int k = 0;
			while (true) {
				k = in.read();
				message = message + (char) k;
				// System.out.println(k);
				if (k == -1) {
					break;
				}
			}
			System.out.println(message);
			s.close();
		} catch (Exception x) {
			System.out.println("Socket reached timeout. Closing socket...");
		}

		String[] dataToFetch = message.split("=");
		for (int i = 11; i < (nTimesSent + 2) * 5; i = i + 5) {
			altitude.add(Integer.parseInt(dataToFetch[i].split("\\s+")[0]));
		}
		for (int i = 13; i < (nTimesSent + 2) * 5; i = i + 5) {
			pressure.add(Double.parseDouble(dataToFetch[i].split("\\s+")[0] + "d"));
		}
		try {
			File altitudeFile = new File("copterData.dat");
			FileOutputStream outFile = new FileOutputStream(altitudeFile);
			for (int i = 0; i < altitude.size(); i++) {
				outFile.write(
						(Integer.toString(altitude.get(i)) + " " + Double.toString(pressure.get(i)) + "\n").getBytes());
			}
			System.out
					.println("Altitude and pressure from the ithakicopter saved at " + altitudeFile.getCanonicalPath());
			outFile.close();
		} catch (Exception x) {
			System.out.println("Error while making file");
		}

	}

	/**
	 * 
	 * Sends nTimes requests so the server can response with measurements from the
	 * OBD-II (SAE J1979) vehicle
	 * 
	 * @param nTimes -how many times do you want measurements from the vehicle
	 */

	public static void vehicle(int nTimes) {

		String message = "";
		ArrayList<int[]> data = new ArrayList<int[]>();

		try {
			Socket s = new Socket();
			InetAddress hostAddress = InetAddress.getByName("ithaki.eng.auth.gr");
			InetSocketAddress socketAddress = new InetSocketAddress(hostAddress, 29078);
			s.connect(socketAddress, 2000);
			// ByteArrayOutputStream buffer=new ByteArrayOutputStream();
			InputStream in = s.getInputStream();
			OutputStream out = s.getOutputStream();

			for (int i = 0; i < nTimes; i++) {
				out.write(("01 1F\r").getBytes());
				out.write(("01 0F\r").getBytes());
				out.write(("01 11\r").getBytes());
				out.write(("01 0C\r").getBytes());
				out.write(("01 0D\r").getBytes());
				out.write(("01 05\r").getBytes());
			}
			int k = 0;

			while (true) {
				k = in.read();
				message = message + (char) k;
				// System.out.println(k);
				if (k == -1) {
					break;
				}
			}

			s.close();
		} catch (Exception x) {
			System.out.println("Socket reached timeout. Closing socket...");
		}

		System.out.println(message);
		String[] dataToFetch = message.split("\\s+");
		// System.out.println(dataToFetch.length);
		for (int i = 0; i < dataToFetch.length - 1; i = i + 20) {
			int j = 0;
			int[] dataToAdd = new int[8];
			dataToAdd[j++] = Integer.parseInt(dataToFetch[i + 2], 16);
			dataToAdd[j++] = Integer.parseInt(dataToFetch[i + 3], 16);
			dataToAdd[j++] = Integer.parseInt(dataToFetch[i + 6], 16);
			dataToAdd[j++] = Integer.parseInt(dataToFetch[i + 9], 16);
			dataToAdd[j++] = Integer.parseInt(dataToFetch[i + 12], 16);
			dataToAdd[j++] = Integer.parseInt(dataToFetch[i + 13], 16);
			dataToAdd[j++] = Integer.parseInt(dataToFetch[i + 16], 16);
			dataToAdd[j++] = Integer.parseInt(dataToFetch[i + 19], 16);

			data.add(dataToAdd);
		}

		try {
			File dataFile = new File("vehicleData.dat");
			FileOutputStream outFile = new FileOutputStream(dataFile);
			for (int i = 0; i < data.size(); i++) {
				for (int j = 0; j < data.get(i).length; j++) {
					outFile.write((Integer.toString(data.get(i)[j]) + " ").getBytes());
				}
				outFile.write("\n".getBytes());
			}
			System.out.println("Altitude and pressure from the ithakicopter saved at " + dataFile.getCanonicalPath());
			outFile.close();
		} catch (Exception x) {
			System.out.println("Error while making file");
		}

	}
	/**
	 * 
	 * A function for user input
	 * 
	 * @return	-the input of the user String
	 */
	
	public static String input() {
		Scanner in = new Scanner(System.in);
		String input = in.nextLine();
		in.close();
		return input;
	}
}